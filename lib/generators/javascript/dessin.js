var _inblocdessin = false;

Blockly.JavaScript['dessin_debut'] = function(block) {
  _inblocdessin = true;
  var instr = Blockly.JavaScript.statementToCode(block, 'bloc');
  _inblocdessin = false;
  var code = 'var dessinDebut_ = function() {\n'+ instr +  '};\n dessinDebut_();\n';
  return code;
};
Blockly.JavaScript['dessin_rouge'] = function(block) {
  var code = 'pixel("r");\n';
  return (_inblocdessin ? code : "");
};
Blockly.JavaScript['dessin_vert'] = function(block) {
  var code = 'pixel("g");\n';
  return (_inblocdessin ? code : "");
};
Blockly.JavaScript['dessin_jaune'] = function(block) {
  var code = 'pixel("y");\n';
  return (_inblocdessin ? code : "");
};
Blockly.JavaScript['dessin_bleu'] = function(block) {
  var code = 'pixel("b");\n';
  return (_inblocdessin ? code : "");
};
Blockly.JavaScript['dessin_avance'] = function(block) {
  var code = 'avance();\n';
  return (_inblocdessin ? code : "");
};
Blockly.JavaScript['dessin_recule'] = function(block) {
  var code = 'recule();\n';
  return (_inblocdessin ? code : "");
};
Blockly.JavaScript['dessin_allera'] = function(block) {
  var value_x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC);
  var value_y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'allera(' + value_x + ',' + value_y + ');\n';
  return (_inblocdessin ? code : "");
};
Blockly.JavaScript['dessin_droite'] = function(block) {
  var code = 'droite();\n';
  return (_inblocdessin ? code : "");
};
Blockly.JavaScript['dessin_gauche'] = function(block) {
  var code = 'gauche();\n';
  return (_inblocdessin ? code : "");
};
