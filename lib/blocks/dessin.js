
'use strict';

// goog.provide('Blockly.Blocks.dessin');  // Deprecated
// goog.provide('Blockly.Constants.Dessin');
//
// goog.require('Blockly.Blocks');


// Blockly.Constants.Dessin.HUE = 20;
// Blockly.Blocks.dessin.HUE = Blockly.Constants.Dessin.HUE;

Blockly.defineBlocksWithJsonArray([
{
  "type": "dessin_debut",
  "message0": "Programme de DESSIN  %1 %2",
  "args0": [
    {
      "type": "input_dummy"
    },
    {
      "type": "input_statement",
      "name": "bloc"
    }
  ],
  "colour": 300,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "dessin_rouge",
  "message0": "ROUGE",
  "previousStatement": null,
  "nextStatement": null,
  "colour": 0,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "dessin_jaune",
  "message0": "JAUNE",
  "previousStatement": null,
  "nextStatement": null,
  "colour": 60,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "dessin_vert",
  "message0": "VERT",
  "previousStatement": null,
  "nextStatement": null,
  "colour": 120,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "dessin_bleu",
  "message0": "BLEU",
  "previousStatement": null,
  "nextStatement": null,
  "colour": 240,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "dessin_avance",
  "message0": "\u2191 AVANCE",
  "previousStatement": null,
  "nextStatement": null,
  "colour": 285,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "dessin_allera",
  "message0": "Aller à %1 %2",
  "args0": [
    {
      "type": "input_value",
      "name": "x",
      "check": "Number"
    },
    {
      "type": "input_value",
      "name": "y",
      "check": "Number"
    }
  ],
  "inputsInline": true,
  "previousStatement": null,
  "nextStatement": null,
  "colour": 285,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "dessin_droite",
  "message0": "\u21B7 Tourne DROITE",
  "previousStatement": null,
  "nextStatement": null,
  "colour": 285,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "dessin_gauche",
  "message0": "\u21B6 Tourne GAUCHE",
  "previousStatement": null,
  "nextStatement": null,
  "colour": 285,
  "tooltip": "",
  "helpUrl": ""
},
{
  "type": "dessin_recule",
  "message0": "\u2193 RECULE",
  "previousStatement": null,
  "nextStatement": null,
  "colour": 285,
  "tooltip": "",
  "helpUrl": ""
}
]);
