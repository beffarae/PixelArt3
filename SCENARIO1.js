var creation = {
        hauteur: 9,
        largeur: 9,
        cycle: 3,
        valeur: [
            [null, null, null, null, null, null, null, null, null],
            [null, null,  "y",  "y",  "y",  "y",  "y", null, null],
            [null,  "y",  "b",  "b",  "y",  "b",  "b",  "y", null],
            [null,  "y",  "b",  "b",  "y",  "b",  "b",  "y", null],
            [null,  "y",  "y",  "y",  "y",  "y",  "y",  "y", null],
            [null,  "y",  "r",  "y",  "y",  "y",  "r",  "y", null],
            [null,  "y",  "y",  "r",  "r",  "r",  "y",  "y", null],
            [null, null,  "y",  "y",  "y",  "y",  "y", null, null],
            [null, null, null, null, null, null, null, null, null]
        ]
    }

var scenario = [{
        intro: "Reproduis le motif ci-dessous",
        cycle: 3,
        hauteur: 4,
        largeur: 5,
        x: 2,
        y: 2,
        valeur: [
            [null, null, null, null, null],
            [null, "r",  "r",  "r",  null],
            [null, "r",  null, null, null],
            [null, null, null, null, null],
        ]
    },
    {
        intro: "Reproduis le motif ci-dessous",
        cycle: 3,
        hauteur: 5,
        largeur: 6,
        x: 2,
        y: 2,
        valeur: [
            [null, null, null, null, null, null],
            [null, null, "r",  "r",  "r",  null],
            [null, "r",  "r",  null, null, null],
            [null, "r",  null, null, null, null],
            [null, null, null, null, null, null],
        ]
    },
    {
        intro: "Reproduis le motif ci-dessous",
        cycle: 3,
        hauteur: 7,
        largeur: 8,
        x: 2,
        y: 2,
        valeur: [
            [null, null, null, null, null, null, null, null],
            [null, null, null, null, "r",  "r",  "r",  null],
            [null, null, null, "r",  "r",  null, null, null],
            [null, null, "r",  "r",  null, null, null, null],
            [null, "r",  "r",  null, null, null, null, null],
            [null, "r",  null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null],
        ]
    },
    {
        intro: "Reproduis le motif ci-dessous",
        cycle: 3,
        hauteur: 13,
        largeur: 14,
        x: 2,
        y: 2,
        valeur: [
            [null, null, null, null, null, null, null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null, null, null, "r",  "r",  "r",  null],
            [null, null, null, null, null, null, null, null, null, "r",  "r",  null, null, null],
            [null, null, null, null, null, null, null, null, "r",  "r",  null, null, null, null],
            [null, null, null, null, null, null, null, "r",  "r",  null, null, null, null, null],
            [null, null, null, null, null, null, "r",  "r",  null, null, null, null, null, null],
            [null, null, null, null, null, "r",  "r",  null, null, null, null, null, null, null],
            [null, null, null, null, "r",  "r",  null, null, null, null, null, null, null, null],
            [null, null, null, "r",  "r",  null, null, null, null, null, null, null, null, null],
            [null, null, "r",  "r",  null, null, null, null, null, null, null, null, null, null],
            [null, "r",  "r",  null, null, null, null, null, null, null, null, null, null, null],
            [null, "r",  null, null, null, null, null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null, null, null, null, null, null, null],
        ]
    },
    {
        intro: "Reproduis le motif ci-dessous",
        cycle: 3,
        hauteur: 11,
        largeur: 32,
        x: 2,
        y: 2,
        valeur: [
            [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null, null, "r",  "r",  "r",  null, null, null, null, null, null, null, "r",  "r",  "r",  null, null, null, null, null, null, null, "r",  "r",  "r",  null],
            [null, null, null, null, null, null, null, "r",  "r",  null, "r",  null, null, null, null, null, null, "r",  "r",  null, "r",  null, null, null, null, null, null, "r",  "r",  null, "r",  null],
            [null, null, null, null, null, null, "r",  "r",  null, null, "r",  null, null, null, null, null, "r",  "r",  null, null, "r",  null, null, null, null, null, "r",  "r",  null, null, "r",  null],
            [null, null, null, null, null, "r",  "r",  null, null, null, "r",  null, null, null, null, "r",  "r",  null, null, null, "r",  null, null, null, null, "r",  "r",  null, null, null, "r",  null],
            [null, null, null, null, "r",  "r",  null, null, null, null, "r",  null, null, null, "r",  "r",  null, null, null, null, "r",  null, null, null, "r",  "r",  null, null, null, null, "r",  null],
            [null, null, null, "r",  "r",  null, null, null, null, null, "r",  null, null, "r",  "r",  null, null, null, null, null, "r",  null, null, "r",  "r",  null, null, null, null, null, "r",  null],
            [null, null, "r",  "r",  null, null, null, null, null, null, "r",  null, "r",  "r",  null, null, null, null, null, null, "r",  null, "r",  "r",  null, null, null, null, null, null, "r",  null],
            [null, "r",  "r",  null, null, null, null, null, null, null, "r",  "r",  "r",  null, null, null, null, null, null, null, "r",  "r",  "r",  null, null, null, null, null, null, null, "r",  null],
            [null, "r",  null, null, null, null, null, null, null, null, "r",  "r",  null, null, null, null, null, null, null, null, "r",  "r",  null, null, null, null, null, null, null, null, "r",  null],
            [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
        ]
   }
];
