function fixedGrid(container, ligne, colonne, id, height) {
    var sideLength = (height - 2) / ligne - 2;

    var table = document.createElement('table');
    table.setAttribute('class', 'mt-1');
    table.style['margin'] = 'auto';
    table.style['background-color'] = '#c8c8c8';
    table.style['border-collapse'] = 'separate';

    // Ajout des cases
    for (var i = 1; i <= ligne; i++) {
        var tr = document.createElement('tr');
        for (var j = 1; j <= colonne; j++) {
            var td = document.createElement('td');
            td.setAttribute('id', id + '_' + i + '-' + j);
            td.style['background-color'] = 'white';
            td.style['width'] = sideLength + 'px';
            td.style['height'] = sideLength + 'px';
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    container.appendChild(table);
}

function responsiveGrid(container, ligne, colonne, id) {
    var table = document.createElement('table');
    table.style['width'] = '100%';
    table.style['margin'] = 'auto';
    table.style['background-color'] = '#c8c8c8';
    table.style['border-collapse'] = 'separate';
    table.style['max-width'] = String(40 * colonne) + 'px';

    for (var i = 1; i <= ligne; i++) {
        var tr = document.createElement('tr');
        for (var j = 1; j <= colonne; j++) {
            var td = document.createElement('td');
            td.setAttribute('id', id + '_' + i + '-' + j);
            td.style['background-color'] = 'white';
            td.innerHTML = '<div style="padding-bottom: 100%;position: relative;">';
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    container.appendChild(table);
}

function colorGrid(id, matrice, clearCursor = false) {
    var colors = {
        'b': '#00f',
        'r': '#f00',
        'g': '#0f0',
        'y': '#ff0',
    };

    for (var i = 0; i < matrice.length; i++) {
        for (var j = 0; j < matrice[i].length; j++) {
            var pixel = document.getElementById(id + '_' + String(i + 1) + '-' + String(j + 1));
            if (matrice[i][j] != null) {
                pixel.style["background-color"] = colors[matrice[i][j]];
            } else {
                pixel.style["background-color"] = 'white';
            }
            if (clearCursor) {
                pixel.children[0].innerHTML = '';
            }
        }
    }
}

function colorPixel(id, couleur, x, y) {
    var colors = {
        'b': '#00f',
        'r': '#f00',
        'g': '#0f0',
        'y': '#ff0'
    };

    var pixel = document.getElementById(id + '_' + y + '-' + x);
    pixel.style["background-color"] = colors[couleur];
}

function logEntry() {
    var i = sessionStorage.getItem('count');
    if (i == null)
        i = 1;
    else
        i = parseInt(i) + 1;
    sessionStorage.setItem('count', i);
    var row = '<td>' + (new Date).toLocaleTimeString() + '</td>';
    for (const item of arguments)
        row += '<td>' + item + '</td>'
    sessionStorage.setItem('L' + ('0000' + i).slice(-5), row);
}

function printlog() {
    var log = Object.entries(sessionStorage).sort();
    var texte = "<h1>Enregistrement de l'activité</h1>\n<table><tbody>\n";
    for (const item of log) {
        if (item[0][0] != 'L') continue;
        texte += '<tr><td>' + parseInt(item[0].substring(1)) + '</td>' + item[1] + '</tr>\n';
    }
    texte += '</tbody></table>';
    var fenetre = window.open("log/log.html");
    setTimeout((function() {
        fenetre.document.getElementById("logactivite").innerHTML = texte;
    }), 500);

}

function exportScenario(intro, cycle, hauteur, largeur, x, y, valeur) {
    var tab = '&nbsp;&nbsp;&nbsp;&nbsp;';
    var br = '<br>';
    var text = tab + '{' + br;

    text += tab + tab + 'intro: "' + intro + '",' + br;
    text += tab + tab + 'cycle: ' + cycle + ',' + br;
    text += tab + tab + 'hauteur: ' + hauteur + ',' + br;
    text += tab + tab + 'largeur: ' + largeur + ',' + br;
    text += tab + tab + 'x: ' + x + ',' + br;
    text += tab + tab + 'y: ' + (hauteur - y + 1) + ',' + br;
    text += tab + tab + 'valeur: [' + br;

    for (var i = 0; i < valeur.length; i++) {
        text += tab + tab + tab + '[';
        for (var j = 0; j < valeur[i].length; j++) {
            if (valeur[i][j] != null) {
                text += '&nbsp;"' + valeur[i][j] + '"';
            } else {
                text += valeur[i][j];
            }

            if (j < valeur[i].length - 1) {
                text += ', ';
            }
        }
        text += ']';
        if (i < valeur.length - 1) {
            text += ',';
        }
        text += br;
    }

    text += tab + tab + ']' + br;
    text += tab + '}' + br;

    return text;
}
