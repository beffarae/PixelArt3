///////////////////////////////////////////////////////////////////////
///////////////////////////////  SCRIPT ///////////////////////////////
///////////////////////////////////////////////////////////////////////

var urlbase = location.href.split('?')[0];
var paramsurl = location.search.substring(1).split('&');
var params = [];
for (var i = 0; i < paramsurl.length; i++) {
    var par = paramsurl[i].split('=');
    params[par[0]] = par[1];
}

var largeur, hauteur, x0, y0;
var etape;
var nom;
var x;
var y;
var dx;
var dy;
var cycle;
var curseuron = true;
var idDessin = 'dessin';
var erreurMouvement = false;

if (params["nom"]) {
    nom = params["nom"];
} else {
    nom = "";
}

if (params["etape"]) {
    etape = params["etape"];
} else {
    etape = -1;
}

if (etape != -1 && scenario[etape]) {
    largeur = scenario[etape]["largeur"];
    hauteur = scenario[etape]["hauteur"];
    x0 = scenario[etape]["x"];
    y0 = hauteur - scenario[etape]["y"] + 1;
    cycle = scenario[etape]["cycle"];
} else {
    if (params["cycle"]) {
        cycle = params["cycle"];
    } else {
        if (creation["cycle"]){
            cycle = creation["cycle"];
        } else {
            cycle = 3;
        }
    }
    if (params["largeur"]) {
        largeur = Math.min(params["largeur"], 30);
    }
    if (params["hauteur"]) {
        hauteur = Math.min(params["hauteur"], 30);
    }
    if (hauteur) {
        if (!largeur) {
            largeur = hauteur;
        }
    } else {
        if (largeur) {
            hauteur = largeur;
        } else {
            largeur = 20;
            hauteur = 20;
        }
    }
    if (params["x"] && params["x"] >= 1 && params["x"] <= largeur) {
        x0 = parseInt(params["x"]);
    } else {
        x0 = Math.floor((largeur + 1) / 2);
    }
    if (params["y"] && params["y"] >= 1 && params["y"] <= hauteur) {
        y0 = parseInt(params["y"]);
    } else {
        y0 = Math.floor((hauteur + 1) / 2);
    }
}

var resultat = [];
for (var i = 0; i < hauteur; i++) {
    resultat[i] = [];
    for (var j = 0; j < largeur; j++) {
        resultat[i][j] = null;
    }
}

Blockly.Msg.LOOPS_HUE = 320;
Blockly.Constants.Loops.HUE = Blockly.Msg.LOOPS_HUE;

var blocklyArea = document.getElementById('blocklyArea');
var blocklyDiv = document.getElementById('blocklyDiv');
var workspace = Blockly.inject(blocklyDiv, {
    toolbox: document.getElementById(cycle == 3 ? 'toolbox' : 'toolbox4'),
    zoom: {
        controls: true,
        wheel: true,
        startScale: 1.0,
        maxScale: 2,
        minScale: 0.5,
        scaleSpeed: 1.2
    },
    trashcan: true
});

var debut = workspace.newBlock("dessin_debut");
debut.initSvg();
debut.moveBy(20,20);
debut.render();

var onresize = function(e) {
    // Compute the absolute coordinates and dimensions of blocklyArea.
    var element = blocklyArea;
    var xb = 0;
    var yb = 0;
    do {
        xb += element.offsetLeft;
        yb += element.offsetTop;
        element = element.offsetParent;
    } while (element);
    // Position blocklyDiv over blocklyArea.
    blocklyDiv.style.left = xb + 'px';
    blocklyDiv.style.top = yb + 'px';
    blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
    blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
};

window.addEventListener('resize', onresize, false);

Blockly.HSV_SATURATION = .9;
Blockly.HSV_VALUE = .9;
Blockly.JavaScript.addReservedWords('code');

workspace.addChangeListener(logevents);

setup();

////////////////////////////////////////////////////////////////////////
//////////////////////////////  FONCTIONS //////////////////////////////
////////////////////////////////////////////////////////////////////////

function highlightBlock(id) {
    workspace.highlightBlock(id);
}

function showCode() {
    // Generate JavaScript code and display it.
    Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
    var code = Blockly.JavaScript.workspaceToCode(workspace);
    alert(code);
}

function initApi(interpreter, scope) {
    var wrapper = function() {
        return interpreter.createPrimitive(avance());
    };
    interpreter.setProperty(scope, 'avance',
        interpreter.createNativeFunction(wrapper));
    wrapper = function() {
        return interpreter.createPrimitive(recule());
    };
    interpreter.setProperty(scope, 'recule',
        interpreter.createNativeFunction(wrapper));
    wrapper = function(x,y) {
        return interpreter.createPrimitive(allera(x,y));
    };
    interpreter.setProperty(scope, 'allera',
        interpreter.createNativeFunction(wrapper));
    wrapper = function() {
        return interpreter.createPrimitive(droite());
    };
    interpreter.setProperty(scope, 'droite',
        interpreter.createNativeFunction(wrapper));
    wrapper = function() {
        return interpreter.createPrimitive(gauche());
    };
    interpreter.setProperty(scope, 'gauche',
        interpreter.createNativeFunction(wrapper));
    wrapper = function(c) {
        c = c ? c.toString() : 0;
        return interpreter.createPrimitive(pixel(c));
    };
    interpreter.setProperty(scope, 'pixel',
        interpreter.createNativeFunction(wrapper));
    wrapper = function(id) {
        id = id ? id.toString() : '';
        return interpreter.createPrimitive(workspace.highlightBlock(id));
    };
    interpreter.setProperty(scope, 'highlightBlock',
        interpreter.createNativeFunction(wrapper));
}

function stepCode() {
    document.getElementById('dessine').disabled = 'disabled';
    document.getElementById('efface').disabled = 'disabled';
    // document.getElementById('spinner').style.display = '';
    Blockly.JavaScript.STATEMENT_PREFIX = 'highlightBlock(%1);\n';
    Blockly.JavaScript.addReservedWords('highlightBlock');
    var code = Blockly.JavaScript.workspaceToCode(workspace);
    var myInterpreter = new Interpreter(code, initApi);

    function nextStep() {
        if (myInterpreter.step()) {
            verifErreur();
            window.setTimeout(nextStep, 50);
        } else {
            if (verifResultat()) {
                logEntry('Dessine pas à pas OK Activité ' + (parseInt(etape) + 1));
                if (etape <= scenario.length - 1) {
                    sessionStorage["C" + (parseInt(etape) + 1)] = "1";
                    document.getElementById('suivant').removeAttribute('disabled');
                }
            } else {
                logEntry('Dessine pas à pas');
            }
            workspace.highlightBlock(null);
            document.getElementById('dessine').disabled = '';
            document.getElementById('efface').disabled = '';
            // document.getElementById('spinner').style.display = 'none';
        }
    }
    nextStep();
}

function runCode() {
    // Generate JavaScript code and run it.
    window.LoopTrap = 1000;
    Blockly.JavaScript.INFINITE_LOOP_TRAP =
        'if (--window.LoopTrap == 0) throw "Infinite loop.";\n';
    var code = Blockly.JavaScript.workspaceToCode(workspace);
    Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
    try {
        eval(code);
        workspace.highlightBlock(null);
    }
    catch (e) {
        alert(e);
    }
    if (verifResultat()) {
        logEntry('Dessine OK Activité ' + (parseInt(etape) + 1));
        if (etape <= scenario.length - 1) {
            sessionStorage["C" + (parseInt(etape) + 1)] = "1";
            document.getElementById('suivant').removeAttribute('disabled');
        }
    } else {
        logEntry('Dessine');
    }
}

function curseurSet() {
    curseuron = document.getElementById('trace').checked;
}

function curseur(forced = false) {
    if (curseuron || forced) {
        var pixel = document.getElementById(idDessin + '_' + y + '-' + x);
        if (pixel != null) {
            pixel.children[0].innerHTML = '<img src="images/arrow.svg" class="img-fluid fleche" style="transform: matrix(' + (-dy) + ',' + (-dx) + ',' + (-dx) + ',' + (-dy) + ',0,0)">';
        }
    }
}

function pixel(couleur) {
    resultat[y - 1][x - 1] = couleur;
    colorPixel(idDessin, couleur, x, y);
    curseur();
}

function avance() {
    x += dx;
    y += dy;
    verifMouvement('avance');
    curseur();
}

function recule() {
    x -= dx;
    y -= dy;
    verifMouvement('recule');
    curseur();
}

function allera (xi,yi) {
  var xp = x;
  var yp = y;
  x = xi;
  y = hauteur - yi + 1;
  verifMouvement('allera', xp, yp);
  curseur();
}

function verifMouvement(operation, xp = 1, yp = 1) {
    if (x <= 0 || y <= 0 || x > largeur || y > hauteur) {
        if (operation == 'avance') {
            x -= dx;
            y -= dy;
        } else if (operation == 'recule') {
            x += dx;
            y += dy;
        } else if (operation == 'allera') {
            x = xp;
            y = yp;
        }
        erreurMouvement = true;
    }
}

function droite() {
    if (dx == 1 && dy == 0) {
        dx = 0;
        dy = 1;
    } else if (dx == -1 && dy == 0) {
        dx = 0;
        dy = -1;
    } else if (dx == 0 && dy == 1) {
        dx = -1;
        dy = 0;
    } else if (dx == 0 && dy == -1) {
        dx = 1;
        dy = 0;
    }
    curseur();
}

function gauche() {
    if (dx == 1 && dy == 0) {
        dx = 0;
        dy = -1;
    } else if (dx == -1 && dy == 0) {
        dx = 0;
        dy = 1;
    } else if (dx == 0 && dy == 1) {
        dx = 1;
        dy = 0;
    } else if (dx == 0 && dy == -1) {
        dx = -1;
        dy = 0;
    }
    curseur();
}

function efface(displayCursor = false) {
    // Cache les anciens messages
    document.getElementById('alertError').style.display = 'none';
    document.getElementById('alertSuccess').style.display = 'none';

    erreurMouvement = false;
    x = x0;
    y = y0;
    dx = 0;
    dy = -1;
    for (var i = 0; i < resultat.length; i++) {
        for (var j = 0; j < resultat[i].length; j++) {
            resultat[i][j] = null;
        }
    }
    colorGrid(idDessin, resultat, true);
    curseur(displayCursor);
    document.activeElement.blur();
}

function dessine() {
    curseurSet();
    efface();
    // Execution du code
    if (document.getElementById('pasapas').checked) {
        stepCode();
    } else {
        runCode();
    }
}

function verifErreur() {
    if (erreurMouvement) {
        document.getElementById('alertError').style.display = 'block';
    }
}

function verifResultat() {
    if (erreurMouvement) {
        document.getElementById('alertError').style.display = 'block';
        return false;
    } else if (etape >= 0 && scenario[etape] && JSON.stringify(resultat) === JSON.stringify(scenario[etape].valeur)) {
        document.getElementById('alertSuccess').style.display = 'block';
        return true;
    } else {
        return false;
    }
}

function nextetape() {
    window.location = urlbase + '?etape=' + (parseInt(etape) + 1) + '&nom=' + nom;
    logEntry('Début activité ' + (parseInt(etape) + 2));
}

function prevetape() {
    window.location = urlbase + '?etape=' + (parseInt(etape) - 1) + '&nom=' + nom;
    logEntry('Début activité ' + etape);
}

function retour(base = 'index.html') {
    window.location = base + '?nom=' + nom;
    logEntry('Retour au menu');
}

function blockAsText(block) {
    var text = block.type;
    if (block.type == "controls_repeat")
        text += '[' + block.getFieldValue('TIMES') + ']';
    var children = block.getChildren(true);
    if (children.length > 1) {
        text += '(' + blockAsText(children[0]);
        for (var i = 1; i < children.length - 1; i++)
            text += ';' + blockAsText(children[i]);
        text += ')';
    }
    if (children.length > 0)
        text += ',' + blockAsText(children[children.length - 1]);
    return text;
}

function workspaceAsText(workspace) {
    var text = '';
    for (const block of workspace.getTopBlocks(true)) {
        if (text != '') text += ' ';
        var pos = block.getRelativeToSurfaceXY();
        text += '[' + pos.x + ',' + pos.y + '] ' + blockAsText(block);
    }
    return text;
}

function logevents(event) {
    var debut;
    if (event.type == "create") {
        logEntry('Création d\'un bloc ' + (workspace.getBlockById(event.blockId)).type,
            workspaceAsText(workspace));
    } else if (event.type == "delete") {
        logEntry('Destruction d\'un bloc', workspaceAsText(workspace));
    } else if (event.type == "move") {
        logEntry('Déplacement d\'un bloc', workspaceAsText(workspace));
    } else if (event.type == "change") {
        logEntry('Modification d\'un bloc', workspaceAsText(workspace));
    } else {
        logEntry('Événement de type ' + event.type);
    }
    var topblocks = workspace.getTopBlocks();
    if (topblocks.length == 0){
        debut = workspace.newBlock("dessin_debut");
        debut.initSvg();
        debut.moveBy(20,20);
        debut.render();
    }
}

function setup() {

    // Gestion du titre
    var titre = document.getElementById('titreActivite');
    // Ajout de la zone de dessin
    var drawingArea = document.getElementById('drawingArea');
    responsiveGrid(drawingArea, hauteur, largeur, idDessin);
    // Gestion du nom de l'utilisateur
    var nameArea = document.getElementById('nameArea');
    nameArea.innerHTML = 'Dessin : ' + decodeURI(nom);

    // Gestion des différences Activités/Création
    if (etape >= 0 && scenario[etape]) { // Cas des Activité
        // Ajout du modèle
        var modelArea = document.getElementById('modelArea');
        responsiveGrid(modelArea, hauteur, largeur, 'modele');
        colorGrid('modele', scenario[etape].valeur);

        titre.innerHTML = 'Activité ' + (parseInt(etape) + 1) + ' : ' + scenario[etape]["intro"];

        // Désactive le boutton précédent
        if (etape == 0) {
            document.getElementById('precedent').setAttribute('disabled', 'true');
        }
        // Désactive le boutton suivant
        if (etape == scenario.length - 1 || !sessionStorage["C" + (parseInt(etape) + 1)]) {
            document.getElementById('suivant').setAttribute('disabled', 'true');
        }

        // Place les éléments trop haut à côté
        if (hauteur >= 2 * largeur) {
            modelArea.setAttribute('class', 'col-6 mb-2 text-center');
            drawingArea.setAttribute('class', 'col-6 mb-2 text-center');
        }
    } else { // Cas de la Création
        document.getElementById('precedent').remove();
        document.getElementById('suivant').remove();
        document.getElementById('modelArea').remove();
        drawingArea.classList.remove('col-sm-6');
        var spanCreation = document.createElement('span');
        spanCreation.innerHTML = 'Création';
        spanCreation.setAttribute('onclick', 'afficherScenario();');
        titre.appendChild(spanCreation);
    }

    efface(true);

    // Paramétrage de blockly
    onresize();
    Blockly.svgResize(workspace);
}

function afficherScenario() {
    var modal = document.getElementById('modalExport');
    modal.style.display = 'block';

    var close = document.getElementById('buttonClose');
    close.addEventListener('click', function() {
        modal.style.display = "none";
    });

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    var content = document.getElementById('contentExport');
    content.innerHTML = exportScenario("Reproduis le motif ci-dessous", cycle, hauteur, largeur, x, y, resultat);
}
