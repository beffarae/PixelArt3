var creation = {
        hauteur: 9,
        largeur: 9,
        cycle: 3,
        valeur: [
            [null, null, null, null, null, null, null, null, null],
            [null, null,  "y",  "y",  "y",  "y",  "y", null, null],
            [null,  "y",  "b",  "b",  "y",  "b",  "b",  "y", null],
            [null,  "y",  "b",  "b",  "y",  "b",  "b",  "y", null],
            [null,  "y",  "y",  "y",  "y",  "y",  "y",  "y", null],
            [null,  "y",  "r",  "y",  "y",  "y",  "r",  "y", null],
            [null,  "y",  "y",  "r",  "r",  "r",  "y",  "y", null],
            [null, null,  "y",  "y",  "y",  "y",  "y", null, null],
            [null, null, null, null, null, null, null, null, null]
        ]
    }

var scenario = [{
        intro: "Reproduis le motif ci-dessous",
        cycle: 3,
        hauteur: 5,
        largeur: 5,
        x: 3,
        y: 2,
        valeur: [
            [null, null, null, null, null],
            [null, null, "r", null, null],
            [null, null, "y", null, null],
            [null, null, "g", null, null],
            [null, null, null, null, null]
        ]
    },
    {
        intro: "Reproduis le motif ci-dessous",
        cycle: 3,
        hauteur: 5,
        largeur: 5,
        x: 2,
        y: 2,
        valeur: [
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, "b", "r", "y", null],
            [null, "g", null, null, null],
            [null, null, null, null, null]
        ]
    },
    {
        intro: "Reproduis le motif ci-dessous",
        cycle: 3,
        hauteur: 5,
        largeur: 5,
        x: 2,
        y: 3,
        valeur: [
            [null, null, null, null, null],
            [null, null, null, null, null],
            [null, "r", "y", "r", null],
            [null, null, null, null, null],
            [null, null, null, null, null]
        ]
    },
    {
        intro: "Reproduis le motif ci-dessous",
        cycle: 3,
        hauteur: 5,
        largeur: 5,
        x: 3,
        y: 4,
        valeur: [
            [null, null, null, null, null],
            [null, null, "b", null, null],
            [null, null, "r", null, null],
            [null, null, "y", null, null],
            [null, null, null, null, null]
        ]
    }
];
